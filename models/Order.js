const mongoose = require('mongoose')
const {ObjectId} = mongoose.Schema

const orderSchema = mongoose.Schema({
  customerId: { 
    type: ObjectId,
    ref: 'Store',
    required: true    
  },
  status:{type: String, default: 'order_placed'}
}, {timestamps: true})


module.exports = mongoose.model('Order', orderSchema)