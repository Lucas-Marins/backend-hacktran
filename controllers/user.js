const User = require('../models/User')

exports.userById = (req, res, next,id) =>{
  User.findById(id).exec((error, user)=>{
    if(error || !user){
      return res.status(400).json({
        error: 'User not found'
      })
    }

    user.salt = undefined
    user.hashed_password = undefined
    req.profile = user
    next()
  })
}



exports.read = (req, res) =>{
  req.profile.hashed_password = undefined
  req.profile.salt = undefined
  return res.json(req.profile)
}
