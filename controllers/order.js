const Order = require('../models/Order')


 exports.index = (req,res) => {
  Order.find({status:{ $ne: 'completed' }}, null, { sort: { 'createdAt': -1 }}).
  populate('customerId', '-password').exec((err, orders)=>{
    if(err){
      return res.status(400).json({error: 'Error'})
    }
   
    res.json(orders)
  })
}

exports.create = (req, res) => {
  const order = new Order(req.body)

  order.save((error, data) =>{
    if(error){
      res.status(400).json({error: 'Error'})
    }

    res.json(data)
  })
}

exports.orderById = (req, res, next, id) =>{
  Order.findById(id).exec((error, order)=>{
    if(error || !order){
      return res.status(400).json({
        error: 'Order not found'
      })
    }
    req.order = order
    next()
  })
}

exports.read = (req, res) =>{
  return res.json(req.order)
}

