const express = require('express')
const router = express.Router()

const {create, index, orderById, read } = require('../controllers/order')


router.post('/order', create )
router.get('/order', index)

router.get('/order/:orderId', read)
router.param('orderId', orderById)


module.exports = router