const express = require('express')
const router = express.Router()

const {userById, read} = require('../controllers/user')

router.get('/secret/:userId', (req,res)=>{
  res.json({
    user:req.profile
  })
})

router.get('/user/:userId', read)
router.param('userId', userById)

module.exports = router
