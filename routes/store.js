const express = require('express')
const router = express.Router()

const {getStores, addStores,listBySearch} = require('../controllers/store')


router.get('/', getStores )
router.post('/', addStores )
router.get('/search', listBySearch)

module.exports = router