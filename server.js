const path = require('path')
const express = require('express')
const dotenv = require('dotenv')
const cors = require('cors')
const mongoose = require('mongoose')

require('dotenv').config()



// Connect DB
mongoose.connect(process.env.DATABASE,{
  useNewUrlParser: true,
  useCreateIndex: true,
  useFindAndModify: false,
  useUnifiedTopology: true
})
.then(()=>{ console.log('DB esta conectado')})

const app = express();

//Body parser
app.use(express.json())

// Enable cors
app.use(cors())

// Set static folder
app.use(express.static(path.join(__dirname, 'public')));


// Routes
app.use('/api', require('./routes/store'))
app.use('/api', require('./routes/auth'))
app.use('/api', require('./routes/order'))
app.use('/api', require('./routes/user'))

const PORT = process.env.PORT || 8000

app.listen(PORT)